FROM python:3.5-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y \
    libpq-dev \
    python-dev \
    build-essential \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /usr/src/app
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 5000

COPY . /usr/src/app
CMD ["python", "run.py"]
