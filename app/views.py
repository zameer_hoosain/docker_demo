import uuid
from app import app

INSTANCE_ID = uuid.uuid4()

@app.route('/')
def index():
  return "Hello, World! [%s]\n" % INSTANCE_ID
