#!/usr/bin/env bash
docker build -t gcr.io/decipher-staging-platform/docker-demo:${BUILD_NUMBER} .
docker build -t gcr.io/decipher-staging-platform/docker-demo:latest .

gcloud docker push gcr.io/decipher-staging-platform/docker-demo:${BUILD_NUMBER}
gcloud docker push gcr.io/decipher-staging-platform/docker-demo:latest
